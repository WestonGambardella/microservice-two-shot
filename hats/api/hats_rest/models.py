from django.db import models

# Create your models here.
class HatVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    location_name = models.CharField(max_length=200)


class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name=models.CharField(max_length=200)
    color=models.CharField(max_length=200)
    image_url=models.URLField(null=True)
    location=models.ForeignKey(
        "HatVO",
        related_name="hat",
        on_delete=models.CASCADE
    )
