from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import HatVO, Hat
import json



class HatVOEncoder(ModelEncoder):
    model = HatVO
    properties = ["import_href", "location_name"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "fabric", "style_name", "location"]
    encoders = {
        "location":HatVOEncoder()
    }

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "image_url",
        "location"
    ]
    encoders = {
        "location":HatVOEncoder()
    }
# Create your views here.


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            location = HatVO.objects.get(import_href=content["location"]) #this is just setting the attr when it sends the data back to the database
            content["location"] = location
        except HatVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location ID"},
                status=400,
            )
        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder = HatDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET"])
def api_delete_hat(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Hat.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except:
            return HttpResponse(status=400)
    else:
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )
