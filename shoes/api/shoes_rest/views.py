
from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import ShoeVO, Shoe
import json


class ShoeVOEncoder(ModelEncoder):
    model = ShoeVO
    properties = ["import_href", "location_name"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model_name", "bin", "color", "URL", "id"]
    encoders = {
        "bin":ShoeVOEncoder()
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "URL",
        "bin"
    ]
    encoders = {
        "bin":ShoeVOEncoder()
    }


@require_http_methods(["GET", "POST"])
def ShoePollView(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin = ShoeVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except ShoeVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bins ID"},
                status=400,
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def ShoePolldelete(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Shoe.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except:
            return HttpResponse(status=400)
