from django.urls import path
from shoes_rest.views import ShoePollView, ShoePolldelete


urlpatterns = [
    path("shoe/", ShoePollView, name="ShoePollView"),
    path("shoe/<int:id>/", ShoePolldelete, name="ShoePolldelete"),
]
