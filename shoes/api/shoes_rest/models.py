from django.db import models


class ShoeVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    location_name = models.CharField(max_length=200)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=101, null=True)
    model_name = models.CharField(max_length=100, null=True)
    color = models.CharField(max_length=50, null=True)
    URL = models.URLField(null=True)
    bin = models.ForeignKey(
        ShoeVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        )
