import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
// import { useEffect } from 'react';


export default function HatDetail() {
    const [data, SetData] = useState([])
    const {hatId} = useParams()

    useEffect(() => {
    const fetchData = async () => {
    const response = await fetch(`http://localhost:8090/api/hats/${hatId}/`)
    if(response.ok) {
        const data = await response.json()
        SetData(data)
    }
    }
    fetchData()
    }, [hatId]);
    return (
        <>
        <h1>{ data.style_name }</h1>
            <div className="card">
                <img src={data.image_url} className="card-img-top" alt="..."></img>
                    <div className="card-body">
                    <h5 className="card-title"> {data.color} </h5>
                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
            </div>
        </>
    )
}
