import React, { useState } from "react";
import { Link } from "react-router-dom";

export async function deleteData(id) {
    const HatUrl = `http://localhost:8090/api/hats/${id}/`
    const response = await fetch(HatUrl, {
      method: "DELETE",
    });
    if (response.ok) {
        window.location.reload(false)
    }
  }


function ListHats(props) {
    return (
        <>
        <div className="row">
        <Link to="create">Submit a new hat!</Link>
        </div >
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style</th>
            <th>Wardrobe Location</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td><Link to={`/hats/${hat.id}`}>{ hat.fabric }</Link></td>
                <td>{ hat.style_name }</td>
                <td>{ hat.location["location_name"] }</td>
                <td> <button onClick={() => deleteData(hat.id)}> Delete Hat</button>  </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
  }

  export default ListHats;
