import React, { useEffect, useState } from 'react';


export default function CreateHat() {
    const [fabric, setFabric] = useState("")
    const [style, setStyle] = useState("")
    const [color, setColor] = useState("")
    const [image_url, setImage] = useState("")
    const [locations, setLocations] = useState([])
    const [location, setLocation] = useState("")


    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/"

        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)
        }
    }
    useEffect(() => {
        fetchData();
      }, []);


      const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.fabric = fabric
        data.style_name = style
        data.color = color
        data.image_url = image_url
        data.location = location

        const HatUrl = "http://localhost:8090/api/hats/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(HatUrl, fetchConfig)
        if (response.ok) {
            setFabric("");
            setStyle("");
            setColor("");
            setImage("");
            setLocation("");
        }
      }


      const handleLocation = (event) => {
        const value = event.target.value
        setLocation(value)
      }


    const handleImageChange = (event) => {
        const value = event.target.value
        setImage(value)
    }


    const handleColoreChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const handleStyleChange = (event) => {
        const value = event.target.value
        setStyle(value)
    }

    const handleFabricChange = (event) => {
        const value = event.target.value
        setFabric(value)
    }


    return (
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Submit a new hat!</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={handleFabricChange} value={fabric} placeholder="Fabric Type" required type="text" name="fabric" id="fabric_name" className="form-control"/>
                <label htmlFor="fabric_type">Fabric Type</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStyleChange} value={style} placeholder="Style" required type="text" name="fabric" id="style_type" className="form-control"/>
                <label htmlFor="style_type">What kind of style is it?</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColoreChange} value={color} placeholder="Color" required type="text" name="fabric" id="color" className="form-control"/>
                <label htmlFor="color">What color is it?</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleImageChange} value={image_url} placeholder="Picture" required type="text" name="fabric" id="image_url" className="form-control"/>
                <label htmlFor="color">Got an image URL?</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocation} value={location} required id="location" name="location" className="form-select">
                  <option value="">Choose a Wardrobe Location</option>
                  {locations.map(location => {
                    return (
                        <option key ={location.href} value={location.href}>
                            {location.closet_name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        </>
    )
}
