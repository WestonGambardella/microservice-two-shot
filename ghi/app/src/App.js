import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ListHats from "./ListHats"
import CreateHat from "./CreateHat"
import HatDetail from './HatDetail'
import Nav from './Nav';
import ShoeList from './shoeList'
import Createshoe from './Createshoe'



export default function App(props) {
  if (props.hats === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<ListHats hats={props.hats}/>}/>
          <Route path="/hats/create" element={<CreateHat />}/>
          <Route path="/hats/:hatId" element={<HatDetail />}/>
          <Route path="/shoes" element={<ShoeList shoes={props.shoes}/>}/>
//        <Route path="/shoes/create" element={<Createshoe />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
