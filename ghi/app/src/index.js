import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


async function loadHats() {
  const response1 = await fetch("http://localhost:8090/api/hats/")
  const response2 = await fetch("http://localhost:8080/api/shoe/")
  if (response1.ok && response2.ok) {
      const data1 = await response1.json();
      const data2 = await response2.json();

      root.render(
        <div className="my-5 container">
        <React.StrictMode>
          <App hats={data1.hats} shoes={data2.shoes} />
        </React.StrictMode>
        </div>
      );
    } else {
      // console.error(response);
    }
  }
  loadHats();
