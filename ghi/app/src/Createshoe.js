import React, { useEffect, useState } from 'react';


export default function Createshoe() {
    const [manufacturer, setmanufacturer] = useState("")
    const [model_name, setmodel_name] = useState("")
    const [color, setcolor] = useState("")
    const [URL, setURL] = useState("")
    const [bins, setbins] = useState([])
    const [bin, setbin] = useState("")


    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/"

        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setbins(data.bins)
        }
    }
    useEffect(() => {
        fetchData();
      }, []);


      const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.manufacturer = manufacturer
        data.model_name = model_name
        data.color = color
        data.URL = URL
        data.bin = bin
        const shoeUrl = "http://localhost:8080/api/shoe/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(shoeUrl, fetchConfig)
        if (response.ok) {
            setmanufacturer("");
            setmodel_name("");
            setcolor("");
            setURL("");
            setbin("");
        }
      }


      const handlebin = (event) => {
        const value = event.target.value
        setbin(value)
      }


    const handleURLChange = (event) => {
        const value = event.target.value
        setURL(value)
    }


    const handlecoloreChange = (event) => {
        const value = event.target.value
        setcolor(value)
    }

    const handlemodelChange = (event) => {
        const value = event.target.value
        setmodel_name(value)
    }

    const handlemanufacturerChange = (event) => {
        const value = event.target.value
        setmanufacturer(value)
    }


    return (
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Submit a new shoe!</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={handlemanufacturerChange} value={manufacturer} placeholder="manufacturer Name" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">manufacturer Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlemodelChange} value={model_name} placeholder="model" required type="text" name="model" id="model_name" className="form-control"/>
                <label htmlFor="model_name">What model is it?</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlecoloreChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">What color is it?</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleURLChange} value={URL} placeholder="Picture" required type="text" name="URL" id="URL" className="form-control"/>
                <label htmlFor="URL">Got an URL?</label>
              </div>
              <div className="mb-3">
                <select onChange={handlebin} value={bin} required id="bin" name="bin" className="form-select">
                  <option value="">Choose a Wardrobe bin</option>
                  {bins.map(bin => {
                    return (
                        <option key ={bin.href} value={bin.href}>
                            {bin.closet_name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        </>
    )
}
