import React, { useState } from "react";
import { Link } from "react-router-dom";
import './index.css';

export async function deleteData(id) {
    const shoeUrl = `http://localhost:8080/api/shoe/${id}/`
    const response = await fetch(shoeUrl, {
      method: "DELETE",
    });
    if (response.ok) {
        window.location.reload(false)
    }
  }


  function ShoeList(props) {
    return (
        <>
        <div className="row">
        <Link to="create">Submit a new shoe!</Link>
        </div >
      <table className="table table-striped">
        <thead>
          <tr>
            <th>manufacturer</th>
            <th>model_name</th>
            <th>Color</th>
            <th>Image</th>
            <th>Wardrobe Bin</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map(Shoe => {
            return (
              <tr key={Shoe.id}>
                <td><Link to={`/shoe/${Shoe.id}`}>{ Shoe.manufacturer }</Link></td>
                <td>{ Shoe.model_name }</td>
                <td>{ Shoe.color }</td>
                <td>
                  <img src={ Shoe.URL } alt=""></img>
                </td>
                <td>{ Shoe.bin["location_name"] }</td>
                <td> <button onClick={() => deleteData(Shoe.id)}> Delete Shoe</button>  </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
  }

  export default ShoeList;
